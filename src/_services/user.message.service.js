import config from '../config';
import { authHeader } from '../_helpers';
import { responseService } from './response.service';
const qs = require('qs');

export const userMessageService = { get , getManyBy};

/**
 * Get 1 message
 *
 * @param      {<type>}  id      The identifier
 * @return     {<type>}  { description_of_the_return_value }
 */
function get(id)
{
    const requestOptions = {  method: 'GET', headers: authHeader() };
    return responseService.fetchRetry(`${config.apiUrl}messages/${id}`, requestOptions, config.retryTimes);
}

/**
 * Gets the many by.
 *
 * @param      {<type>}  requestOptionsBody  The request options body
 * @return     {<type>}  The many by.
 */
function getManyBy(requestOptionsBody)
{
    const requestOptions = { method: 'GET', headers: authHeader() };
    let _qs = qs.stringify(requestOptionsBody);

    return responseService.fetchRetry(`${config.apiUrl}messages?${_qs}`, requestOptions, config.retryTimes);
}