import config from '../config';
import { authHeader } from '../_helpers';
import { responseService } from './response.service'

const qs = require('qs');
export const googleadsAccountService = { get, getRuntime};

/**
 * Read Account data
 *
 * @param      Number  clientId            The client identifier
 * @param      Object  requestOptionsBody  The request options body
 * @return     Callback
 */
function get(clientId, requestOptionsBody)
{
    const requestOptions = { method: 'GET', headers: authHeader() };
    let _qs = qs.stringify(requestOptionsBody);

    return responseService.fetchRetry(`${config.apiUrl}googleads/accounts/${clientId}?${_qs}`, requestOptions, config.retryTimes);
}

/**
 * Gets the runtime of Account.
 *
 * @param      Number  clientId  The client identifier
 * @return     Callback  The runtime.
 */
function getRuntime(clientId)
{
    const requestOptions = { 
        method: 'GET', 
        headers: authHeader()
    };

    return responseService.fetchRetry(`${config.apiUrl}googleads/accounts/${clientId}/runtime`, requestOptions, config.retryTimes);
}

/**
 * Validate response , catch error
 *
 * @param      Resource  response  The response
 * @return     Callback
 */
function handleResponse(response)
{
    return response.text().then(text => {

        const data = text && JSON.parse(text);

        if ( ! response.ok)
        {
            if (response.status === 401) {
                // logout();
                // location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }
        
        return data;
    });
}