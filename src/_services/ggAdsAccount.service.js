import config from '../config';
import { authHeader } from '../_helpers';
const qs = require('qs');

export const ggAdsAccountService = { get , getManyBy};

function get(contract_id)
{
    return 'contract get one fake';
}

function getManyBy(requestOptionsBody)
{
    const requestOptions = { 
        method: 'GET', 
        headers: authHeader()
    };
    
    let _qs = qs.stringify(requestOptionsBody);

    return fetch(`${config.apiUrl}googleads/accounts?${_qs}`, requestOptions)
    .then(handleResponse)
    .then(result => {
        return result;
    });
}

function handleResponse(response)
{
    return response.text().then(text => {

        const data = text && JSON.parse(text);

        if ( ! response.ok)
        {
            if (response.status === 401) {
                // logout();
                // location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }
        
        return data;
    });
}