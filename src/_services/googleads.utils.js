import config from '../config';
export const googleadsUtils = { MicroTo, adGroupCriterionKeywordMatchTypeToString, dollarToVN, convertPropsItem};

function MicroTo(value) { return value / 1000000 }

function adGroupCriterionKeywordMatchTypeToString (value) { return config.keywordMatchTypEnum[value].text }

function dollarToVN(value, rate) { return value*rate }

function toLocaleString (value) {
    if(_.isUndefined(value)) return 0;
    return value.toLocaleString("en-En");
}

function convertPropsItem (dataItem, column) {

    let value = dataItem[column.name];

    if(_.isEqual(config.fieldNameConfig[column.name].type, "adGroupCriterionKeywordMatchType")) return adGroupCriterionKeywordMatchTypeToString(value);

    if(_.isEqual(config.fieldNameConfig[column.name].type, "MicroNumber")) value = MicroTo(value);

    let mustFixedColumns = ['AverageCpc','Ctr','CostMicros','AveragePosition','CostPerConversion','ConversionsFromInteractionsRate','ConversionsValuePerCost'];

    if(_.has(column, 'formatStyle') && column.formatStyle == 'percentage') value*= 100;

    if(_.includes(mustFixedColumns, column.name))
    {
        value = _.round(value, 2).toFixed(2);
        if(_.includes(['AverageCpc','CostMicros','ConversionsValuePerCost', 'CostPerConversion'], column.name))
        {
            switch (dataItem.CurrencyCode) {
                case "VND": value = _.round(value); break;
                default: break;
            }
        }
    }


    if(_.includes(['Number', 'MicroNumber'], config.fieldNameConfig[column.name].type)) value = toLocaleString(value);


    if(_.has(column, 'formatStyle'))
    {
        switch (column.formatStyle) {
            case "percentage": value = value + " %"; break;
            case "currency": value = value + (_.isEqual(dataItem.CurrencyCode, 'USD') ? ' $' : ' đ'); break;
        }
    }

    return value;
}