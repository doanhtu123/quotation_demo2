import config from '../config';
import { authHeader } from '../_helpers';
import { responseService } from './response.service';
const qs = require('qs');

export const contractService = { get , countContracts, getManyBy};

function get(contractId)
{
    const requestOptions = {  method: 'GET', headers: authHeader() };
    return responseService.fetchRetry(`${config.apiUrl}contracts/${contractId}`, requestOptions, config.retryTimes);
}

function countContracts()
{
    const requestOptions = { method: 'GET', headers: authHeader() };
    return responseService.fetchRetry(`${config.apiUrl}contracts/countByType`, requestOptions, config.retryTimes);
}

function getManyBy(requestOptionsBody)
{
    const requestOptions = { method: 'GET', headers: authHeader() };
    let _qs = qs.stringify(requestOptionsBody);

    return responseService.fetchRetry(`${config.apiUrl}contracts?${_qs}`, requestOptions, config.retryTimes);
}