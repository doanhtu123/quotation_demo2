import config from '../config';
import { authHeader } from '../_helpers';
import { responseService } from './response.service'
const qs = require('qs');

export const googleadsAdGroupService = { get };

function get(clientId, requestOptionsBody)
{
    const requestOptions = { 
        method: 'GET', 
        headers: authHeader()
    };
    
    let _qs = qs.stringify(requestOptionsBody);

    return responseService.fetchRetry(`${config.apiUrl}googleads/accounts/${clientId}/adgroups?${_qs}`, requestOptions, config.retryTimes);
}