export default {
    apiUrl: process.env.VUE_APP_API_URL,
    fieldNameConfig: {
		Id: { 
			text : "ID tài khoản",
			value: "Id",
			type: "Number",
			allowedChartSelected: false,
		},
		DescriptiveName: { 
			text : "Tên tài khoản",
			value: "DescriptiveName",
			type: "String",
			allowedChartSelected: false,
		},
		TrackingUrlTemplate: { 
			text : "Tracking Url Template",
			value: "TrackingUrlTemplate",
			type: "String",
			allowedChartSelected: false,
		},
		CurrencyCode: { 
			text : "Tiền tệ",
			value: "CurrencyCode",
			type: "String",
			allowedChartSelected: false,
		},
		Date: { 
			text : "Ngày",
			value: "Date",
			type: "String",
		},

		CampaignName: { 
			text : "Chiến dịch",
			value: "CampaignName",
			type: "String",
			allowedChartSelected: false,
		},

		AdGroupName: { 
			text : "Nhóm QC",
			value: "AdGroupName",
			type: "String",
			allowedChartSelected: false,
		},

		adGroupCriterionKeywordText: { 
			text : "Từ khóa",
			value: "adGroupCriterionKeywordText",
			type: "String",
			allowedChartSelected: false,
		},

		adGroupCriterionKeywordMatchType: { 
			text : "Loại từ khóa",
			value: "adGroupCriterionKeywordMatchType",
			type: "adGroupCriterionKeywordMatchType",
			allowedChartSelected: false,
		},

		AverageCpc: { 
			text : "C.Phí/ Click",
			value: "AverageCpc",
			type: "MicroNumber",
			allowedChartSelected: true,
		},
		AveragePosition: { 
			text : "Vị trí TB",
			value: "AveragePosition",
			type: "Number",
			allowedChartSelected: true,
		},
		Clicks: { 
			text : "Clicks",
			value: "Clicks",
			type: "Number",
			allowedChartSelected: true,
		},
		Conversions: { 
			text : "C. Đổi",
			value: "Conversions",
			type: "Number",
			allowedChartSelected: true,
		},
		ConversionsFromInteractionsRate: { 
			text : "Tỷ lệ c.đổi",
			value: "ConversionsFromInteractionsRate",
			type: "Number",
			allowedChartSelected: true,
		},
		ConversionsValue: { 
			text : "Giá trị c.đổi",
			value: "ConversionsValue",
			type: "Number",
			allowedChartSelected: true,
		},
		ConversionsValuePerCost: { 
			text : "ROAS (giá trị c.đổi/ chi phí)",
			value: "ConversionsValuePerCost",
			type: "Number",
			allowedChartSelected: true,
		},
		CostMicros: { 
			text : "Chi phí",
			value: "CostMicros",
			type: "MicroNumber",
			allowedChartSelected: true,
		},
		Ctr: { 
			text : "CTR",
			value: "Ctr",
			type: "Number",
			allowedChartSelected: true,
		},
		Impressions: { 
			text : "Hiển thị",
			value: "Impressions",
			type: "Number",
			allowedChartSelected: true,
		},
		valuePerConversion: { 
			text : "C.Phí/ c.đổi",
			value: "valuePerConversion",
			type: "MicroNumber",
			allowedChartSelected: true,
		},

		CostPerConversion: { 
			text : "C.Phí/ c.đổi",
			value: "CostPerConversion",
			type: "MicroNumber",
			allowedChartSelected: true,
		},

		AverageCpm: { 
			text : "CPM",
			value: "AverageCpm",
			type: "MicroNumber",
			allowedChartSelected: true,
		},

		ActiveViewCpm: { 
			text : "vCPM",
			value: "ActiveViewCpm",
			type: "MicroNumber",
			allowedChartSelected: true,
		},

		VideoViews: { 
			text : "Views",
			value: "VideoViews",
			type: "Number",
			allowedChartSelected: true,
		},

		AverageCpv: { 
			text : "CPV",
			value: "AverageCpv",
			type: "MicroNumber",
			allowedChartSelected: true,
		},
    },
    keywordMatchTypEnum: {
		0: { 
			value : 'UNSPECIFIED',
			text : "Chưa xác định"
		},
		1: { 
			value : 'UNKNOWN',
			text : "Không xác định"
		},
		2: { 
			value : 'EXACT',
			text : "Chính xác"
		},
		3: { 
			value : 'PHRASE',
			text : "Cụm từ"
		},
		4: { 
			value : 'BROAD',
			text : "Rộng"
		}
    },
    retryTimes: 4
}