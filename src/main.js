import Vue from 'vue'
import './plugins/vuetify'
import VueRouter from 'vue-router'
import App from './App.vue'
import { store } from './_store/store'
import router from './router'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { fas } from '@fortawesome/free-solid-svg-icons'
import VueResource from 'vue-resource'
import 'material-design-icons-iconfont/dist/material-design-icons.css' // Ensure you are 
// setup fake backend
import { configureFakeBackend } from './_helpers';
configureFakeBackend();

Vue.use(VueRouter)
Vue.config.productionTip = false

Vue.use(VueResource);
Vue.component('font-awesome-icon', FontAwesomeIcon) // Register component globally
library.add(fas) // Include needed icons.

new Vue({
	render: h => h(App),
	store,
	router,
	
}).$mount('#app')
Vue.use(Vuetify, {
	iconfont: 'fas',
	icons: {
		'percent': 'fas fa-percentage',

	}
  }).$mount('#app')