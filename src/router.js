import Vue from 'vue'
import Router from 'vue-router'

import HomePage from './views/Home'
import LoginPage from './views/Login'
import Quotation from './views/inc/Quotation'

import GoogleadsContract from './views/GoogleadsContract'
import GoogleadsAccount from './views/GoogleadsAccount'
import Messages from './views/Messages'

Vue.use(Router)

const router = new Router({
    mode: 'history', // tránh load lại trang khi thay đổi
    routes: [
        { 
            path: '/home', name: "HomePage", component: HomePage,
            children: [
                { 
                    name: "GoogleadsContract",
                    path: "googleads/contract/:id", 
                    component: GoogleadsContract,
                    props: true
                },
                { 
                    name: "GoogleadsAccount",
                    path: "googleads/account/:id", 
                    component: GoogleadsAccount,
                    props: true
                },
                { 
                    name: "Messages",
                    path: "messages", 
                    component: Messages,
                    props: true
                },
            ]
        },
        { path: '/login', component: LoginPage },

        // otherwise redirect to home
        { path: '*', redirect: '/' },
        { path: '/quotation', name:'quotation', component: Quotation },
        // { path: '/quotation2', name:'quotation', component: QuotationTu },
    ]
});

// router.beforeEach((to, from, next) => {

//     // redirect to login page if not logged in and trying to access a restricted page
//     const publicPages = ['/login'];
//     const authRequired = !publicPages.includes(to.path); 
//     const loggedIn = localStorage.getItem('user');
//     if (authRequired && !loggedIn) return next('/login');

//     next();
// })

export default router;