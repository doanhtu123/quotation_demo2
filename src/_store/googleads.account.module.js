import { contractService } from '../_services/googleads.account.service';

const countContracts = JSON.parse(localStorage.getItem('countContracts'));
export const contracts = {
    namespaced: true,
    state: {
        all: {},
        counter: (countContracts && countContracts.status == 200 && (new Date(countContracts.dateExpired)) > (new Date())) ? { types : countContracts.data} : {},
        selected : null,
    },
    actions: {
        countContracts({ commit }) {
            commit('countContractRequest');
            contractService.countContracts().then( result => commit('countContractSuccess', result), error => commit('countContractFailure', error) );
        },
        setSelected({commit}, contractId){
            commit('setSelected', contractId)
        }
    },
    mutations: {
        countContractRequest(state) { state.counter = { loading : true }; },
        countContractSuccess(state, result) { state.counter = {  loading : false, types: result.data }; },
        countContractFailure(state, error) { state.counter = { error }; },

        setSelected(state, contractId){ state.selected = contractId; }
    }
}