import Vue from 'vue'
import Vuex from 'vuex'

import { alert } from './alert.module'
import { authentication } from './authentication.module'
import { users } from './users.module'
import { contracts } from './contracts.module'
import { globalFilterings } from './global.filtering.module'
import { sharedData } from './global.shareddata.module'

Vue.use(Vuex)

export const store = new Vuex.Store({
	modules: {
		alert,
		authentication,
		users,
		contracts,
		globalFilterings,
		sharedData
	}
})