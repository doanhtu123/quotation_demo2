export const sharedData = {
    namespaced: true,
    state: {
        contract: null,
        adsAccount: null,
    },
    actions: {
        setContract({commit}, contract) { commit('setContract', contract) },
        setAdsAccount({commit}, adsAccount) { commit('setAdsAccount', adsAccount) },
    },
    mutations: {
        setContract(state, contract) { state.contract = contract },
        setAdsAccount(state, adsAccount) { state.adsAccount = adsAccount },
    }
}