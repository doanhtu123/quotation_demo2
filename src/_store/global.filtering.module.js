
export const globalFilterings = {
    namespaced: true,
    state: {
        contractId: null,
        contract: null,
        clientId: null,
        dateRange: { startDate: null, endDate: null },
        advertisingChannelType : [],
        campaignIds: [],
        adGroupIds: [],
    },
    actions: {
        setContractId({commit}, contractId) { 
            commit('setContractId', contractId)
        },
        setContract({commit}, contract) { commit('setContract', contract)},
        setClientId({commit}, clientId) { commit('setClientId', clientId);},
        setDateRange({commit}, dateRange) { commit('setDateRange', dateRange);},
        setAdvertisingChannelType({commit}, advertisingChannelType) { commit('setAdvertisingChannelType', advertisingChannelType);},
        setCampaignIds({commit}, campaignIds) { commit('setCampaignIds', campaignIds);},
        setAdGroupIds({commit}, adGroupIds) { commit('setAdGroupIds', adGroupIds);},
    },
    mutations: {
        setContractId(state, contractId) { state.contractId = contractId; },
        setContract(state, contract) { state.contract = contract; },
        
        setClientId(state, clientId) { state.clientId = clientId; },
        setDateRange(state, dateRange) { state.dateRange = dateRange;},

        setAdvertisingChannelType(state, advertisingChannelType) { state.advertisingChannelType = advertisingChannelType; },
        setCampaignIds(state, campaignIds) { state.campaignIds = campaignIds; },
        setAdGroupIds(state, adGroupIds) { state.adGroupIds = adGroupIds; },
    }
}